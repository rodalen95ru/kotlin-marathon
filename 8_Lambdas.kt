fun containsEven(collection: Collection<Int>): Boolean =
        collection.any { number: Int -> number%2 == 0 }
